package io.androidpro.architect

sealed class OrderIntent {
    object RefreshOrder: OrderIntent()
}