@file:OptIn(ExperimentalMaterial3Api::class)

package io.androidpro.architect

import android.os.Bundle
import android.util.Log
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import io.androidpro.architect.ui.theme.ArchitectAppsTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ArchitectAppsTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    OrderListScreen()
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterialApi::class)
@Composable
fun OrderListScreen(
    viewModel: OrderListViewModel = OrderListViewModel(FakeRepository)
) {
    val uiState by viewModel.uiState.collectAsState()
    Scaffold(
        topBar = {
            TopAppBar(title = { Text("Marketplace") })
        }
    ) { padding ->
        OrderList(
            padding = padding,
            isRefreshing = uiState.isRefreshing,
            state = uiState.orderListState,
            onRefreshPulled = {
                viewModel.handleIntent(OrderIntent.RefreshOrder)
            }
        )
        //OrderDetails("0", padding)
    }
}


@ExperimentalMaterialApi
@Composable
fun OrderList(
    padding: PaddingValues,
    isRefreshing: Boolean,
    state: OrderListState,
    onRefreshPulled: () -> Unit,
) {
    val pullRefreshState = rememberPullRefreshState(
        refreshing = isRefreshing,
        onRefresh = {
            onRefreshPulled.invoke()
        })

    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(padding)
            .pullRefresh(pullRefreshState)
        ,
        contentAlignment = Alignment.TopCenter
    ) {
        when (state) {
            OrderListState.Loading -> {
                LoadingAnimation()
            }
            is OrderListState.Data -> {
                LazyColumn(
                    verticalArrangement = Arrangement.spacedBy(16.dp)
                ) {
                    items(state.list) { order ->
                        OrderItemCard(order) {
                            //OrderList: clicked $order
                        }
                    }
                }
            }
            is OrderListState.Error -> {
                ErrorImage()
            }
        }
        PullRefreshIndicator(refreshing = isRefreshing, state = pullRefreshState)
    }
}

@Composable
fun OrderItemCard(order: Order, clicked: (Order) -> Unit) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clickable { clicked(order) }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            Text(
                text = "Order ID: ${order.orderId}",
                fontWeight = FontWeight.Bold,
                fontSize = 18.sp
            )
            Text(text = "Customer: ${order.customerName}")
            Text(text = "Restaurant: ${order.restaurantName}")
            Text(text = "Status: ${order.orderStatus}")
        }
    }
}


@Composable
fun OrderDetails(orderId: String, padding: PaddingValues) {

    var order by remember { mutableStateOf<Order?>(null) }

    LaunchedEffect(Unit) {
        val orderById = withContext(Dispatchers.IO) {
            FakeRepository.getById(orderId)
        }
        order = orderById
    }

    if (order != null) {
        // Display the order details
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
                .padding(padding)
        ) {

            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                verticalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                Text(
                    text = "Order ID: ${order!!.orderId}",
                    style = MaterialTheme.typography.headlineSmall
                )
                Text(text = "Customer: ${order!!.customerName}")
                Text(text = "Customer Address: ${order!!.customerAddress}")
                Text(text = "Customer Contact: ${order!!.customerContact}")
                Text(text = "Restaurant: ${order!!.restaurantName}")
                Text(text = "Restaurant Address: ${order!!.restaurantAddress}")
                Text(text = "Special Instructions: ${order!!.specialInstructions ?: "None"}")
                Text(text = "Order Status: ${order!!.orderStatus}")

                // Displaying Order Items
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 16.dp),
                    verticalArrangement = Arrangement.spacedBy(4.dp)
                ) {
                    Text(text = "Items:", style = MaterialTheme.typography.headlineSmall)
                    order!!.orderItems.forEach { item ->
                        Text(text = "${item.quantity}x ${item.itemName} - \$${item.price}")
                    }
                }
            }
        }
    }


}
