package io.androidpro.architect

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class OrderListViewModel(private val repo: FakeRepository): ViewModel() {

    private val _uiState: MutableStateFlow<UiState> = MutableStateFlow(UiState())
    val uiState: StateFlow<UiState> = _uiState.asStateFlow()

    init {
        getOrders()
    }
    
     private fun getOrders() {
        viewModelScope.launch {
            val state = try {
                val list = repo.getAll()
                OrderListState.Data(
                    list = list,
                )
            } catch (e: Throwable) {
                OrderListState.Error(e)
            }
            _uiState.update { it.copy(
                orderListState = state,
                isRefreshing = false
            ) }
        }
    }

    fun handleIntent(intent: OrderIntent) {
        when (intent) {
            OrderIntent.RefreshOrder -> onRefreshPulled()
        }
    }

    private fun onRefreshPulled() {
        viewModelScope.launch {
            _uiState.update { it.copy(
                isRefreshing = true,
            ) }
            getOrders()
        }
    }

    /**
     * Actions:
     * - open the screen
     * - refresh using button
     * - (optional) - swipe to refresh
     * - open details click
     *
     * Data:
     * - list of orders
     * - error
     * - loading state
     *
     */
}