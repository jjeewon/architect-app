package io.androidpro.architect

data class UiState (
    val orderListState: OrderListState = OrderListState.Loading,
    val isRefreshing: Boolean = false,
)

sealed interface OrderListState {
    object Loading: OrderListState

    data class Data(
        val list:List<Order>,
    ): OrderListState

    data class Error(val throwable: Throwable): OrderListState
}